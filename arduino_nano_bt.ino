#include <Wire.h>
#include <DFRobot_LIS2DH12.h>

DFRobot_LIS2DH12 LIS;

int16_t x;
int16_t y;
int16_t z;
int16_t step_;

int wait = 25;

void setup() {
  Wire.begin();
  Serial.begin(9600);
  while(!Serial);
  delay(100);

  // define resolution of the sensor
  while(LIS.init(LIS2DH12_RANGE_16GA) == -1){
    Serial.println("No I2C devices found");
    delay(500);
  }

  step_ = 0;
}

void loop() {
  LIS.readXYZ(x, y, z);
  LIS.mgScale(x, y, z);

  step_ += 1;

  print_position(x, y, z, step_);
  delay(wait);
}

void print_position(int16_t x, int16_t y, int16_t z, int16_t step_){
  Serial.print(x);
  Serial.print(" ");
  Serial.print(y);
  Serial.print(" ");
  Serial.print(z);
  Serial.print(" ");
  Serial.println(step_);
}


